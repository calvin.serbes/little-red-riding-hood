// boutton scroll to 100 view height * nombre d'utilisation
////////////////////////////////////////////////////////////

// const scroll = document.querySelector('.btn_scroll');
// let page = 0;

// scroll.addEventListener('click', () => {
//     window.scrollTo({ top: window.innerHeight * page, behavior: "smooth"});
//     page++
// })

////////////////////////////////////////////////////////////
// boutton scroll to 100 view height
const scroll = document.querySelector('.btn_scroll');
const scroll_2 = document.querySelector('.btn_scroll_2');
const scroll_3 = document.querySelector('.btn_scroll_3');

const scroll_back = document.querySelector('.scroll_back');
const scroll_back_2 = document.querySelector('.scroll_back_2');
const scroll_back_3 = document.querySelector('.scroll_back_3');

const second_view = document.querySelector('.second_view');
const t_1 = document.querySelector('.t_1');
const t_2 = document.querySelector('.t_2');

function scrollMagic(page){
    window.scrollTo({ top: window.innerHeight * page , behavior: "smooth"});
}

scroll.addEventListener('click', e=> {
    scrollMagic(1);
    t_1.style.opacity = "0.7";
})
scroll_2.addEventListener('click', e=> {
    scrollMagic(2);
    t_2.style.opacity = "0.7";
})
scroll_3.addEventListener('click', e=> {scrollMagic(3)})

scroll_back.addEventListener('click', e=> {scrollMagic(0)})
scroll_back_2.addEventListener('click', e=> {scrollMagic(1)})
scroll_back_3.addEventListener('click', e=> {scrollMagic(2)})

// faire apparaitre le continue en activant le son
const active_song_for_continue = document.querySelector('.ambiance_song');

active_song_for_continue.addEventListener('play', () =>{
    scroll.style.opacity = '0.2';
    console.log('cc');
})
