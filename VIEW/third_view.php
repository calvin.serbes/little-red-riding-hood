<div class="third_view section">

    <div class="txt2">
        <div class="t_2 p_color">
            <p>
                This guy freaked me out a bit, I preferred to avoid him.
                I saw him one day, from afar, smiling and sinking, as often, into the forest.
                It was late that day. I couldn't sleep and I was alone.
                It was raining and there was a storm. What an idea to go out that night?
            </p>
        </div>
        <div class="btn_scroll_3">enter the forest..</div>
    </div>

    <div class="img_chap_red">
        <img width="300" src="../image/chap_grandmawolf.jpg" alt="Le petit chaperon rouge">
    </div>
    
    <div class="brouillard">
        <img width="1500" height="1200" src="../image/nuage.png" alt="brouillard">
    </div>
    
    <div class="scroll_back_2">back ?</div>


</div>