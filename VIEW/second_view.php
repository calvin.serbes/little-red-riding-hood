<div class="second_view section">

    <div class="btn_scroll_2">continue.</div>


    <div class="txt1">
        <div class="t_1 p_color">
            <p>
                The young man still went to the forest every day, where he no longer met anyone.
                He took in the sun, sometimes suffered the rain, but above all he soaked up the calm that reigned there.
                Alone, forgetting that voice that tormented him.
                Every day was the same for him. He looked tired, weird, lonely and dreamy.
            </p>
        </div>
    </div>

    <div class="scroll_back">back ?</div>

</div>