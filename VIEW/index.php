<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Le Petit Chaperon Rouge - La vrai histoire</title>
    <!-- // link css -->
    <link rel="stylesheet" href="style.css">
</head>
    <body>
        <div id="fullpage">
        <?php 
            include_once "first_view.php";
            include_once "second_view.php";
            include_once "third_view.php";
            include_once "fourth_view.php";
        ?>
        </div>
        <!-- faire en sorte que le scroll ne fonctionne pas sur le site -->

        <!-- // link script js -->
        <script src="../app.js"></script>
    </body>
</html>