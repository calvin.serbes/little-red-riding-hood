<div class="fourth_view">
    <div class="background_forest">

        
        
        <div class="tree_1">
            <img width="" src="../image/tree.png" alt="">
        </div>
        
        <div class="tree_2">
            <img width="1000" src="../image/tree3.png" alt="">
        </div>
        
        <div class="scroll_back_3">back ?</div>
        
    </div>
    
    <div class="txt3">
        <div class="t_3 p_color">
            <p>
                The young man was alone in the darkness of the woods, a torch in his hand,
                the sound of the rain against the foliage and the roar of the storm were soothing to him.
                He suddenly heard the creaking of a branch and a voice that say:
                "I am here, look at me, turn your lantern towards me."
            </p>
        </div>
    </div>
</div>