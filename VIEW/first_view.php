<div class="first_view section" id="first">

	<div class="indice1">
		<p>
			Hear to move forward
		</p>
	</div>

	<div class="audio">
		<audio controls class="ambiance_song">
			  <source src="../son/dark_stormy.mp4"  type="audio/mp4">
		</audio> 
	</div>

	<div class="title_one">

		<h1 class="principal_title">
			The Little Red
		</h1>
		<p class="riding_hood">
			Riding Hood
		</p>
		<p class="story">
			a story of horror, blood & death.
		</p>
	</div>

	<div class="btn_scroll">Continue ?</div>

	<div class="blood_one">
		<img width="800" src="../image/blood_spash4.png" alt="éclaboussure de sang">
	</div>

</div>
